const std = @import("std");
const clap = @import("clap");
const allocator = std.heap.page_allocator;
const fmt = std.fmt;
const math = std.math;
const process = std.process;
const stderr = std.io.getStdErr().writer();
const stdout = std.io.getStdOut().writer();

const params = [_]clap.Param(clap.Help){
    clap.parseParam("-h, --help             Display this help and exit.") catch unreachable,
    clap.parseParam("-f, --frets <NUM>      The number of frets to calculate.") catch unreachable,
    clap.parseParam("-s, --scale <NUM>      The scale length to calculate.") catch unreachable,
};


pub fn main() anyerror!void {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(clap.Help, &params, .{ .diagnostic = &diag }) catch |err| {
        // Report useful error and exit
        diag.report(std.io.getStdErr().writer(), err) catch {};
        return err;
    };
    defer args.deinit();

    if (args.flag("--help")) {
        stderr.print("Usage: {s} ", .{"zfb"}) catch unreachable;
        clap.usage(stderr, &params) catch unreachable;
        stderr.print("\nFlags: \n", .{}) catch unreachable;
        clap.help(stderr, &params) catch unreachable;
        std.process.exit(0);
    }

    const scale: f64 = if (args.option("--scale")) |s| scale: {
        if (fmt.parseFloat(f64, s)) |num| {
            break :scale num;
        } else |e| {
            try stderr.print("{s}\n", .{e});
            return;
        }
    } else 25.5;

    const frets: u8 = if (args.option("--frets")) |f| frets: {
        if (fmt.parseInt(u8, f, 10)) |num| {
            break :frets num;
        } else |e| {
            try stderr.print("{s}\n", .{e});
            return;
        }
    } else 24;

    try stdout.print("Scale Length: {d}\n------------------\nFret\t| Length\n", .{scale});
    var i: u8 = 1;
    while (i <= frets) {
        const factor = math.pow(f64, 2.0, (@intToFloat(f64, i) / 12.0));
        //try stdout.print("{d:.4}\n", .{factor});
        try stdout.print("{d}\t| {d:.4}\n", .{i, scale / factor});
        i += 1;
    }
}
